import sys
import random
import subprocess

from PySide2.QtWidgets import QApplication

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        self.sessionID = random.randint(000000000000000000,999999999999999999)
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(580, 200)
        MainWindow.setMinimumSize(QSize(580, 200))
        MainWindow.setMaximumSize(QSize(580, 200))
        MainWindow.setCursor(QCursor(Qt.ArrowCursor))
        icon = QIcon()
        icon.addFile(u"../Desktop/u+1f9dc.png", QSize(), QIcon.Normal, QIcon.Off)
        MainWindow.setWindowIcon(icon)
        MainWindow.setWindowOpacity(1)
        MainWindow.setStyleSheet(u"background-color:#000000;")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.pushButton = QPushButton(self.centralwidget)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setGeometry(QRect(480, 160, 86, 27))
        self.pushButton.setStyleSheet(u"border:none; background-color:#eeee00; color:#000000;")
        self.lineEdit = QLineEdit(self.centralwidget)
        self.lineEdit.setObjectName(u"lineEdit")
        self.lineEdit.setGeometry(QRect(120, 70, 441, 26))
        self.lineEdit.setStyleSheet(u"border:none; background-color:#5500aa; color:white;")
        self.label = QLabel(self.centralwidget)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(50, 70, 58, 18))
        self.label.setStyleSheet(u"color:#ffffff;")
        self.lineEdit_2 = QLineEdit(self.centralwidget)
        self.lineEdit_2.setObjectName(u"lineEdit_2")
        self.lineEdit_2.setGeometry(QRect(120, 100, 441, 26))
        self.lineEdit_2.setStyleSheet(u"border:none; background-color:#5500aa; color:white;")
        self.label_2 = QLabel(self.centralwidget)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setGeometry(QRect(50, 100, 58, 18))
        self.label_2.setStyleSheet(u"color:#ffffff;")
        self.label_3 = QLabel(self.centralwidget)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setGeometry(QRect(10, 0, 231, 21))
        self.label_3.setStyleSheet(u"color:#ffffff;")
        self.label_4 = QLabel(self.centralwidget)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setGeometry(QRect(10, 20, 58, 18))
        self.label_4.setStyleSheet(u"color:#ffffff;")
        self.label_5 = QLabel(self.centralwidget)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setGeometry(QRect(10, 170, 400, 18))
        self.label_5.setStyleSheet(u"color:#ffffff;")
        MainWindow.setCentralWidget(self.centralwidget)
        self.pushButton.clicked.connect(self.sendFile)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"OCTOS Troubleshooting Utilities - Zap v1", None))
        self.pushButton.setText(QCoreApplication.translate("MainWindow", u"Send File", None))
        self.lineEdit.setText("")
        self.label.setText(QCoreApplication.translate("MainWindow", u"Server:", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"File path:", None))
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"OCTOS Troubleshooting Utilities", None))
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"Zap v1", None))
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"Session ID: {0}".format(self.sessionID), None))
    # retranslateUi

    def sendFile(self):
        server = self.lineEdit.text()
        filepath = self.lineEdit_2.text()
        file1 = open(filepath,"r")
        encodedFile = subprocess.check_output(["gpg","--symmetric","--batch","--passphrase",str(self.sessionID)],stdin=file1)
        file1.close()
        ncProcess = subprocess.Popen(["nc",server,"42004"],stdin=subprocess.PIPE)
        ncProcess.stdin.write(bytes(encodedFile))
        ncProcess.stdin.close()
        print("Sent")
        self.lineEdit_2.setText("")

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()

    sys.exit(app.exec_())
