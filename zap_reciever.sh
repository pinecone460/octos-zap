# uwusage: ./zap_reciever.sh name@server session_id destination_directory

close_socket() {
    ssh -S octos-zap -O cancel $server
    ssh -S octos-zap -O exit $server
    echo "Closed socket."
    trap - SIGINT SIGTERM
    kill -- -$$
}
trap close_socket SIGINT SIGTERM
server=$1
mkdir $3
ssh -M -S octos-zap -fNT -R \*:42004:localhost:42005 $1
ncat -lkp 42005 -c "gpg --decrypt --batch --passphrase $2 > $3/\$(date +%s)"

ssh -S octos-zap -O cancel $1
ssh -S octos-zap -O exit $1

echo "Closed socket."
trap - SIGINT SIGTERM
